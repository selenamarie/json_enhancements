EXTENSION    = json_enhancements
EXTVERSION   = $(shell grep default_version $(EXTENSION)_with_hstore.control | sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/")

DATA         = $(filter-out $(wildcard sql/*--*.sql),$(wildcard sql/*.sql))
DOCS         = $(wildcard doc/*.md)
USE_MODULE_DB = 1
ALL_OBJS = $(patsubst %.c,%.o,$(wildcard src/*.c))
ifdef NOHSTORE
  TESTS        = $(filter-out test/sql/json_hstore.sql, $(wildcard test/sql/*.sql))
  REGRESS_OPTS = --inputdir=test --outputdir=test \
	  --load-language=plpgsql \
	  --load-extension=json_enhancements 
  OBJS         = $(filter-out src/hstore_to_json.o, $(ALL_OBJS))
else
  TESTS        = $(wildcard test/sql/*.sql)
  REGRESS_OPTS = --inputdir=test --outputdir=test \
	  --load-language=plpgsql \
	  --load-extension=hstore --load-extension=json_enhancements 
  OBJS         = $(ALL_OBJS)
  # If you use this SHLIB_LINK line then you don't need hstore preloaded
  # but you are then tied to using the same location as your build system.
  # If you don't use it, add
  #           shared_preload_libraries = '$libdir/hstore' 
  # to your postgresql.conf
  # It's easier to run the tests if you use this line.
  SHLIB_LINK   = $(shell $(PG_CONFIG) --pkglibdir)/hstore.so

endif
REGRESS      = $(patsubst test/sql/%.sql,%,$(TESTS))
MODULE_big   = $(EXTENSION)
PG_CONFIG    = pg_config

all: sql/$(EXTENSION)--$(EXTVERSION).sql

ifdef NOHSTORE
sql/$(EXTENSION)--$(EXTVERSION).sql: sql/$(EXTENSION)_no_hstore.sql
	cp $< $@
$(EXTENSION).control: $(EXTENSION)_no_hstore.control
	cp $< $@
else
sql/$(EXTENSION)--$(EXTVERSION).sql: sql/$(EXTENSION).sql
	cp $< $@
$(EXTENSION).control: $(EXTENSION)_with_hstore.control
	cp $< $@
endif

DATA_built = sql/$(EXTENSION)--$(EXTVERSION).sql
DATA = $(filter-out sql/$(EXTENSION)--$(EXTVERSION).sql, $(wildcard sql/*--*.sql))
EXTRA_CLEAN = sql/$(EXTENSION)--$(EXTVERSION).sql $(EXTENSION).control

PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

# we put all the tests in a test subdir, but pgxs expects us not to, darn it
override pg_regress_clean_files = test/results/ test/regression.diffs test/regression.out tmp_check/ log/
