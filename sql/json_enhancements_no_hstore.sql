-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION json_enhancements" to load this file. \quit

CREATE FUNCTION json_array_element(the_json json, element integer)
RETURNS json
AS 'MODULE_PATHNAME' , 'json_array_element'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_array_element_text(the_json json, element integer)
RETURNS text
AS 'MODULE_PATHNAME' , 'json_array_element_text'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_object_field(json, text)
RETURNS json
AS 'MODULE_PATHNAME' , 'json_object_field'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_object_field_text(json, text)
RETURNS text
AS 'MODULE_PATHNAME' , 'json_object_field_text'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_extract_path(the_json json, variadic path_elements text[])
RETURNS json
AS 'MODULE_PATHNAME' , 'json_extract_path'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_extract_path_text(the_json json, variadic path_elements text[])
RETURNS text
AS 'MODULE_PATHNAME' , 'json_extract_path_text'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_extract_path_op(the_json json, path_elements text[])
RETURNS json
AS 'MODULE_PATHNAME' , 'json_extract_path'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_extract_path_text_op(the_json json, path_elements text[])
RETURNS text
AS 'MODULE_PATHNAME' , 'json_extract_path_text'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_object_keys(the_json json)
RETURNS SETOF text
AS 'MODULE_PATHNAME' 
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_array_length(the_json json)
RETURNS int
AS 'MODULE_PATHNAME' 
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_each(the_json json, key out text, out value json)
RETURNS SETOF record
AS 'MODULE_PATHNAME' 
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_each_text(the_json json, key out text, value out text)
RETURNS SETOF record
AS 'MODULE_PATHNAME' 
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_array_elements(the_json json) 
RETURNS TABLE (value json)
AS 'MODULE_PATHNAME' 
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_populate_record(base anyelement, from_json json, use_json_as_text boolean DEFAULT false)
RETURNS anyelement
AS 'MODULE_PATHNAME' 
LANGUAGE C IMMUTABLE;

CREATE FUNCTION json_populate_recordset(base anyelement, from_json json, use_json_as_text boolean DEFAULT false)
RETURNS SETOF anyelement
AS 'MODULE_PATHNAME' 
LANGUAGE C IMMUTABLE;

CREATE OPERATOR -> (
       PROCEDURE = json_object_field,
       LEFTARG = json, RIGHTARG = text
);

CREATE OPERATOR -> (
       PROCEDURE = json_array_element,
       LEFTARG = json, RIGHTARG = int
);

CREATE OPERATOR ->> (
       PROCEDURE = json_object_field_text,
       LEFTARG = json, RIGHTARG = text
);

CREATE OPERATOR ->> (
       PROCEDURE = json_array_element_text,
       LEFTARG = json, RIGHTARG = int
);

CREATE OPERATOR #> (
       PROCEDURE = json_extract_path_op,
       LEFTARG = json, RIGHTARG = text[]
);

CREATE OPERATOR #>> (
       PROCEDURE = json_extract_path_text_op,
       LEFTARG = json, RIGHTARG = text[]
);

CREATE FUNCTION to_json(anyelement)
RETURNS json
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT IMMUTABLE;

CREATE FUNCTION json_agg_transfn(internal, anyelement)
RETURNS internal
AS 'MODULE_PATHNAME'
LANGUAGE C IMMUTABLE;

CREATE FUNCTION json_agg_finalfn(internal)
RETURNS json
AS 'MODULE_PATHNAME'
LANGUAGE C IMMUTABLE;

CREATE AGGREGATE json_agg(anyelement) (
    SFUNC = json_agg_transfn,
    FINALFUNC = json_agg_finalfn,
    STYPE = internal
);
    
